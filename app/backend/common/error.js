import _ from 'lodash';
import BbPromise from 'bluebird';

export let errorCodes = {
	OK: 0,
	NotFound: -1,
	TaskNotFound: -101,
	InvalidParameter: -2,
	DatabaseError: -3,
	DatabaseErrorCannotCreateTask: -301,
	DatabaseErrorCannotUpdateTask: -302,
	DatabaseErrorCannotDeleteTask: -303,
	DatabaseErrorCannotGetTasks: -304
};

export class Error {

	name = undefined;
	message = undefined;
	code = undefined;

	constructor( name, message, code, extra = null ) {
		this.name = name.toLowerCase();
		this.message = message;
		this.code = code;
		if( !_.isNil( extra ) ) this.extra = extra;
	}
}

export let error = ( name, message, code, extra )=> {
	return new Error( name, message, code, extra );
};

export let rejectedError = ( name, message, code, extra )=> {
	return BbPromise.reject( error( name, message, code, extra ) );
};

export let success = ( extra )=> {
	return new Error( 'success', 'OK', 0, extra );
};