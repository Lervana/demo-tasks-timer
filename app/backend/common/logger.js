import log4js from 'log4js';
import config from 'config';

log4js.clearAppenders();
log4js.loadAppender('console');
log4js.loadAppender('file');

log4js.addAppender(log4js.appenders.console(), 'default-production');
log4js.addAppender(log4js.appenders.console(), 'default-development');
log4js.addAppender(log4js.appenders.console(), 'default-test');
log4js.addAppender(log4js.appenders.file('logs/demo-tasks-timer-prod.log'), 'default-production');
log4js.addAppender(log4js.appenders.file('logs/demo-tasks-timer-dev.log'), 'default-development');
log4js.addAppender(log4js.appenders.file('logs/demo-tasks-timer-test.log'), 'default-test');

function getLogger( name, logLevel ) {
	name = `${name}-${ process.env.NODE_ENV }`;
	let logger = log4js.getLogger(name);
	logger.setLevel( logLevel );
	return logger;
}

export let DefaultLogger = getLogger( 'default', config.app.log_level );
