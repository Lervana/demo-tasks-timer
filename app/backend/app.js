import config from 'config';
import Server from './server/server';

let server = new Server( config.app.name, config.app.port, process.env.NODE_ENV === 'production' );
server.start();