import express from 'express';
import { DefaultLogger as log } from './../common/logger';

let getProdServer = ( setup )=> {
	log.info( 'Creating prod web server' );
	let server = express();
	setup( server, ( cb )=> {
		fs.readFile( '_build/index.html', 'utf8', cb );
	} );
	return server;
};

export default getProdServer;