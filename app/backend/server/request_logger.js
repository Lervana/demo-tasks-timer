import { DefaultLogger as log } from '../common/logger';

let requestLogger= function (req, res, next) {
	req.log = log;
	next()
};

export default requestLogger;