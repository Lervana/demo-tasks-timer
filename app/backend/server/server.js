import Joi from 'joi';
import express from 'express';
import BbPromise from 'bluebird';
import requestLogger from './request_logger';
import bodyParser from 'body-parser';
import { sequelize } from './../db_models/index';
import { DefaultLogger as log } from './../common/logger';

import task_list from './../routes/task_list';
import task_create from './../routes/task_create';
import task_update from './../routes/task_update';
import task_delete from './../routes/task_delete';

BbPromise.promisifyAll( Joi );

class WebServer {

	isProd = false;
	webApp = undefined;

	constructor( setup, isProd = false ) {
		this.isProd = isProd;
		let getServer = ( process.env.NODE_ENV === 'development' && !isProd ) ? require( './dev_web_server.js' ) : require( './prod_web_server.js' );
		this.webApp = getServer.default( setup );
	}

	start = ( name, port )=> {
		this.webApp.listen( port, ( err, result )=> {
			if( err ) log.error( `Unable to start ${ name } server:`, err );
			else log.info( `${ name } server is listening on ${port}` );
		} );
	};

	stop = ()=> {
		if( this.isProd ) {
			return new BbPromise( ( resolve, reject )=> {
				this.webApp.close( resolve );
			} );
		}
		else {
			return new BbPromise( ( resolve, reject )=> {
				this.webApp.listeningApp.close( resolve );
				this.webApp.close( resolve );
			} );
		}
	};
}

export default class Server {

	static METHODS = {
		GET: 0,
		POST: 1,
		PUT: 2,
		DELETE: 3
	};

	initialized = false;

	running = false;
	closing = false;
	app = undefined;
	webApp = undefined;
	name = undefined;
	port = undefined;

	exit = ( signal )=> {
		if( !this.closing ) {
			log.info( `Caught interrupt signal ${signal} - exiting...` );
			this.stop();
		}
	};

	processInitialize = ()=> {
		process.on( 'uncaughtException', function( e ) {
			log.error( "Uncaught exception:", e );
		} );
		process.on( 'SIGINT', this.exit.bind( this, 'SIGINT' ) );
		process.on( 'SIGTERM', this.exit.bind( this, 'SIGTERM' ) );
		process.on( 'SIGQUIT', this.exit.bind( this, 'SIGQUIT' ) );
	};

	setupApi = ()=> {
		this.app = express();
		this.app.use( bodyParser.text( { type: 'text/html', limit: '1mb' } ) );
		this.app.use( bodyParser.urlencoded( { extended: true, limit: '1mb' } ) );
		this.app.use( bodyParser.json( { limit: '1mb' } ) );
		this.app.use( requestLogger );
		this.addApiRoute( Server.METHODS.GET, '/tasks', task_list );
		this.addApiRoute( Server.METHODS.POST, '/tasks', task_create );
		this.addApiRoute( Server.METHODS.PUT, '/tasks', task_update );
		this.addApiRoute( Server.METHODS.DELETE, '/tasks', task_delete );
		log.info( 'Api setup done' );
	};

	setup = ( webServer )=> {
		webServer.use( '/api', this.app );
		log.info( 'Web setup done' );
	};

	setupWebServer = ( isProd )=> {
		this.webApp = new WebServer( this.setup, isProd );
		this.running = true;
		this.closing = false;
	};

	serverInitialize = ( isProd )=> {
		this.setupApi();
		this.setupWebServer( isProd );
	};

	constructor( name, port, isProd = false ) {
		if( !this.initialized ) {
			this.name = name;
			this.port = port;

			this.processInitialize();
			this.serverInitialize( isProd );
			this.initialized = true;
		}
	}

	start = ()=> {
		if(this.initialized) this.webApp.start( this.name, this.port );
		else log.error( 'Server must be initialized');
	};

	closeSequelize() {
		return new BbPromise( ( resolve, reject )=> {
			if( sequelize ) {
				log.info( "Shutting down database" );
				sequelize.close();
				return resolve( 'OK' );
			}
			else reject( 'Sequelize not connected' );
		} );
	}

	stop = ()=> {
		if( this.running && !this.closing ) {
			this.closing = true;
			return this.closeSequelize().then( ( result )=> {
				log.info( result );
			}, ( err )=> {
				log.error( err );
			} ).finally( ()=> {
				this.webApp.stop();
				process.exit();
				this.running = false;
			} );
		}
		else {
			log.error( `Cannot close server if it is not running` );
		}
	};

	addApiRoute( method, uri, cb ) {
		switch( method ) {
			case Server.METHODS.GET:
				this.app.get( uri, cb );
				break;
			case Server.METHODS.POST:
				this.app.post( uri, cb );
				break;
			case Server.METHODS.PUT:
				this.app.put( uri, cb );
				break;
			case Server.METHODS.DELETE:
				this.app.delete( uri, cb );
				break;
			default:
				throw new Error( `Method ${method} not defined` );
		}
	}
}