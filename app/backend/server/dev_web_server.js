import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import devConfig from '../../_config/webpack.config.dev';
import { DefaultLogger as log } from './../common/logger';

let getDevServer = ( setup )=> {
	log.info( 'Creating dev web server' );
	let devCompiler = webpack( devConfig );
	return new WebpackDevServer( devCompiler, { ...devConfig.devServer, setup: setup } );
};

export default getDevServer;