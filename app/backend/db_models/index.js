import config from 'config';
import Sequelize from 'sequelize';
import { DefaultLogger as log} from './../common/logger';

import TaskModel from './task';

let dbConnection = ()=> {
	let instance = new Sequelize( config.database.name, config.database.username, config.database.password, {
		host: config.database.host,
		port: config.database.port,
		dialect: config.database.dialect,
		native: config.database.native,
		logging: function( ...args ) {
			log.trace( ...args );
		},
		pool: {
			max: 20,
			min: 1,
			idle: 60000
		},
		define: {
			timestamps: true,
			underscored: true,
			paranoid: true
		}
	} );
	return instance;
};

export let sequelize = dbConnection();
export let Task = TaskModel( sequelize );