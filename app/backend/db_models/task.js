import _ from 'lodash';
import { rejectedError, errorCodes } from '../common/error';
import Sequelize from 'sequelize';

export default ( sequelize )=> {
	return sequelize.define( 'task', {
		description: { type: Sequelize.STRING },
		start: { type: Sequelize.DATE },
		stop: { type: Sequelize.DATE },
		duration: { type: Sequelize.INTEGER }
	}, {
		classMethods: {
			getById( id, transaction ){
				return this.findOne( { where: { id: id } }, { transaction } ).then( ( task )=> {
					return _.isNil( task ) ? rejectedError( 'task-not-found', `Task not found for id ${ id }`, errorCodes.TaskNotFound ) : task;
				} )
			}
		}
	} );
};
