import _ from 'lodash';
import { sequelize, Task } from './../db_models';
import { error, errorCodes, success } from './../common/error';
import { handleErrors } from './helpers';

let get_tasks = ( req, res )=>handleErrors( req, res, ( req, res )=> {
	return Task.findAll().then( ( result )=> {
		if( _.has( req, 'log' ) ) req.log.info( 'Tasks listed' );
		return success( { tasks: result } )
	} ).catch( ( err )=> {
		return error( { code: errorCodes.DatabaseErrorCannotGetTasks }, redirect, res );
	} );
} );

export default get_tasks;