import _ from 'lodash';
import Joi from 'joi';
import config from 'config';
import { sequelize, Task } from './../db_models';
import { error, errorCodes, success } from './../common/error';
import { handleErrors } from './helpers';

let validate = ( req )=>Joi.validateAsync( req.body, Joi.object().keys( {
	id: Joi.number().integer().required()
} ) );

let delete_task = ( req, res )=>handleErrors( req, res, ( req, res )=> {
	return validate( req ).then( ( data )=> {
		return sequelize.transaction().then( ( transaction )=> {
			return Task.findOne( { where: { id: data.id, }, transaction } ).then( ( task )=> {
				if( !_.isNil( task ) ) {
					return task.destroy().then( ()=> {
						return transaction.commit().then( ()=> {
							if( _.has( req, 'log' ) ) req.log.info( 'Task deleted' );
							return success();
						} );
					} );
				} else {
					throw new Error('Cannot delete not existing task');
				}
			} ).catch( ( err )=> {
				return transaction.rollback().finally( ()=> {
					return error( 'database-error', err.message, errorCodes.DatabaseErrorCannotDeleteTask );
				} );
			} );
		} );
	}, ( err )=> {
		let msg = _.get( err, 'details.0.message' );
		return error( 'invalid-parameter', msg, errorCodes.InvalidParameter );
	} );
} );

export default delete_task;