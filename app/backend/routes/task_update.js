import _ from 'lodash';
import Joi from 'joi';
import { sequelize, Task } from './../db_models';
import { errorCodes, error, success } from './../common/error';
import { handleErrors } from './helpers';

let validate = ( req )=>Joi.validateAsync( req.body, Joi.object().keys( {
	id: Joi.number().integer().required(),
	description: Joi.string().allow(null).max( 150 ).required()
} ) );

let update_task = ( req, res )=>handleErrors( req, res, ( req, res )=> {
	return validate( req ).then( ( data )=> {
		return sequelize.transaction().then( ( transaction )=> {
			return Task.findOne( { where: { id: data.id, }, transaction } ).then( ( task )=> {
				let result = {
					id: task.id,
					old_description: task.description,
					new_description: undefined
				};

				if( !_.isNil( task ) ) {
					let taskData = { description: data.description };
					return task.update( taskData, transaction ).then( ( updatedTask )=> {
						return transaction.commit().then( ()=> {
							if( _.has( req, 'log' ) ) req.log.info( 'Task updated' );
							result.new_description = updatedTask.description;
							return success( result );
						} );
					} );
				} else {
					throw new Error( 'Cannot update not existing task' );
				}
			} ).catch( ( err )=> {
				return transaction.rollback().finally( ()=> {
					return error( 'database-error', err.message, errorCodes.DatabaseErrorCannotUpdateTask );
				} );
			} );
		} );
	}, ( err )=> {
		let msg = _.get( err, 'details.0.message' );
		return error( 'invalid-parameter', msg, errorCodes.InvalidParameter );
	} );
} );

export default update_task;