import _ from 'lodash';
import Joi from 'joi';
import config from 'config';
import moment from 'moment';
import validator from 'validator';
import { sequelize, Task } from './../db_models';
import { errorCodes, error, success } from './../common/error';
import { handleErrors } from './helpers';

let validate = ( req )=>Joi.validateAsync( req.body, Joi.object().keys( {
	description: Joi.string().max( 150 ),
	start: Joi.date().required(),
	stop: Joi.date().required()
} ) );

let create_task = ( req, res )=>handleErrors( req, res, ( req, res )=> {
	return validate( req ).then( ( data )=> {
		if( validator.isAfter( data.start.toString(), data.stop.toString() ) ) {
			return error( 'invalid-parameter', 'Stop cannot be before start', errorCodes.InvalidParameter );
		}

		return sequelize.transaction().then( ( transaction )=> {
			let taskData = {
				description: data.description,
				start: data.start,
				stop: data.stop,
				duration: moment( data.stop ).diff( moment( data.start ), 's' )
			};
			return Task.create( taskData, { transaction: transaction } ).then( ( task )=> {
				return transaction.commit().then( ()=> {
					if( _.has( req, 'log' ) ) req.log.info( 'Task created' );
					return success( { task } );
				} );
			} ).catch( ( err )=> {
				return transaction.rollback().finally( ()=> {
					return error( { code: errorCodes.DatabaseErrorCannotCreateTask }, redirect, res );
				} );
			} );
		} );
	}, ( err )=> {
		let msg = _.get( err, 'details.0.message' );
		return error( 'invalid-parameter', msg, errorCodes.InvalidParameter );
	} );
} );

export default create_task;