import _ from 'lodash';

let requestCounter = 0;

export let logRequest = ( req )=> {
	requestCounter++;
	if( _.has( req, 'log' ) ) {
		let request = {
			headers: req.headers,
			originalUrl: req.originalUrl,
			method: req.method,
			body: req.body
		};

		req.log.info( `Request #${requestCounter}: ${ JSON.stringify( request ) }` );
	}
	return requestCounter;
};

export let logResponse = ( res, requestId, logger )=> {
	if( !_.isNil( logger ) ) {
		logger.info( `Response #${requestId}: ${ JSON.stringify( res ) }` );
	}
};

export let logError = ( error, requestId, logger )=> {
	if( !_.isNil( logger ) ) {
		logger.error( `Request #${requestId}`, error.message );
		logger.trace( `Request #${requestId}`, error );
	}
	else {
		console.error( `Request #${requestId}`, error );
	}
};


export let handleErrors = ( req, res, action )=> {
	let requestId = logRequest( req );
	let response = {};

	return action( req, res ).then( ( result )=> {
		response = `Result: ${ JSON.stringify( result ) }`;
		res.send( result );
		return result;
	} ).catch( ( err )=> {
		logError( err, requestId, req.log );
		res.send( err );
		return err;
	} ).finally( ()=> {
		logResponse( response, requestId, req.log );
	} );
};