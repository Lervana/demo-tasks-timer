Required
-----
- Docker
- PostgreeSQL
- Prefered system macOS

Project setup:
-----
1. Create env file -> .env in demo-tasks-timer/app:
   ```
   DB_USERNAME=username
   DB_PASSWORD=password
   DB_DATABASE=taskstimer
   DB_HOST=localhost
   DB_PORT=5432
   DB_DIALECT=postgres
   DB_NATIVE=true
   
   SERVER_PORT=6002
   BASE_URL=http://localhost:6002
   BASE_API_URL=http://localhost:6002/api
   
   LOG_LEVEL=TRACE
   ```
2. Create env file -> .env.test in demo-tasks-timer/app:
   ```
   SERVER_PORT=7002
   BASE_URL=http://localhost:7002
   BASE_API_URL=http://localhost:7002/api
   ALLOW_REQUESTS_RETRY=true
   ```

3. Run docker.
4. Start containers:
    - cd demo-tasks-timer 
    - docker-compose up
5. Create database *taskstimer* and *testtaskstimer*
6. Install packages:
   - cd demo-tasks-timer/app/
   - npm install
7. Create _dist:
   - cd demo-tasks-timer/app/
   - gulp
8. Run migrations:
   - cd demo-tasks-timer/app/
   - ./seq.sh db:migrate
   - ./seq.sh db:migrate --env test

Start development environment:
-----

1. Run docker.
2. Start containers:
    - cd demo-tasks-timer 
    - docker-compose up
3. Start dev app:
   - cd demo-tasks-timer/app/
   - gulp watch
   - ./run_dev.sh
   
Start test environment:
-----
1. Run docker.
2. Start containers:
    - cd demo-tasks-timer 
    - docker-compose up
3. Start dev app:
   - cd demo-tasks-timer/app/
   - gulp watch
   - ./run_test.sh
   - ./run_tests.sh
   
Translations
-----
To create translation file (template.pot):
1. cd demo-tasks-timer/app/
2. gulp pot
3. Use pot file to create translations e.g. in Poedit
4. Save translations files in demo-tasks-timer/app/frontend/locales/raw (e.g. pl.mo and pl.po)
5. cd demo-tasks-timer/app/
6. gulp po2json        
      