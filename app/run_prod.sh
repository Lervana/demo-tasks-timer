#!/bin/bash
cd -P "$( dirname "${BASH_SOURCE[0]}" )"

export NODE_ENV=production
export NODE_CONFIG_DIR=./../app/_dist/_config
./node_modules/.bin/nodemon -w ./_dist -i ./_dist/test ./_dist/backend/app.js
