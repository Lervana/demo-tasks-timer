'use strict';
var BbPromise = require( 'bluebird' );

module.exports = {
	up: function( queryInterface, Sequelize ) {
		return queryInterface.createTable( 'tasks', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: Sequelize.INTEGER
			},
			description: {
				type: Sequelize.STRING
			},
			start: {
				type: Sequelize.DATE,
				allowNull: false
			},
			stop: {
				type: Sequelize.DATE,
				allowNull: false
			},
			duration: {
				type: Sequelize.INTEGER,
				allowNull: false
			},
			created_at: {
				allowNull: false,
				type: Sequelize.DATE
			},
			updated_at: {
				allowNull: false,
				type: Sequelize.DATE
			},
			deleted_at: {
				allowNull: true,
				type: Sequelize.DATE
			}
		} );
	},
	down: function( queryInterface, Sequelize ) {
		return BbPromise.all( [
			queryInterface.dropTable( 'tasks' )
		] );
	}
};