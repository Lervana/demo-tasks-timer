import React, { Component } from 'react';
import { translate } from 'react-i18next';
import { observer, inject } from 'mobx-react';

import Snackbar from 'material-ui/Snackbar';

import '../../styles/web.scss';

@translate( [ 'translation' ], { wait: true } )
@inject( 'notifier' )
@observer
class Web extends Component {

	constructor( props, context ) {
		super( props, context );
	}

	render() {
		return (
			<Snackbar open={this.props.notifier.open}
			          message={this.props.notifier.message}
			          autoHideDuration={4000}
			          onRequestClose={this.props.notifier.close}
			/>);
	}
}

export default Web;