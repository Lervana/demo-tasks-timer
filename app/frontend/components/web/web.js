import React, { Component } from 'react';
import { translate } from 'react-i18next';

import Header from './web_header/web_header';
import Notifier from './../common/common_notifier';

import '../../styles/web.scss';

@translate( [ 'translation' ], { wait: true } )
class Web extends Component {

	constructor( props, context ) {
		super( props, context );
	}

	render() {
		return (
			<div className="web">
				<Notifier/>
				<Header/>
				<div>{this.props.children}</div>
			</div>
		);
	}
}

export default Web;