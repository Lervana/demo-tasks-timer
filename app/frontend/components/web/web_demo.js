import React, { Component } from 'react';
import { observer } from 'mobx-react';

import Logo from '../../images/heroes.jpg';

import '../../styles/web_demo.scss';

@observer
class Demo extends Component {

	constructor( props, context ) {
		super( props, context );
	}

	render() {
		return (
			<div className="public">
				<img src={ Logo } alt="Logo" className="logo"/>
				<div className="row text-justify row-full container">
					<div className="column gr-3 gr-12@mobile">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Vestibulum nec
						congueex. Nunc tempor non sem vel aliquet. Proin turpis purus, mollis id est vitae, ultrices
						tinciduntturpis.Nulla acodio placerat, auctor orci in, aliquam magna. Curabitur a nisl sem.
						Maecenas alectus atellus finibus malesuada. Mauris vehicula erat ut neque viverra, in pulvinar
						nibhimperdiet. Maecenas at turpis commodo, viverra leo a, elementum ante. Nam sit amet
						pharetraligula, vel mattis est. Morbi aliquam dolor lorem, in bibendum nunc lacinia vel. In eu
						purus euaugue laoreet tempus. In hac habitasse platea dictumst. Morbi id suscipit purus. Morbi
						lacusaugue, malesuada condimentum ultrices et,mattis eudui. Curabitur at dictum eros, sed
						posuere ex. Fusce laoreet, ex ac viverra finibus, nisi diamfeugiatnulla, quis feugiat diam eros
						vitae tortor.Maecenas vel enim tortor. Quisque eget egestas quam.Quisque augue leo, tempus ac
						urna nec,malesuada vulputate orci. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					</div>
					<div className="column gr-6 gr-12@mobile">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Vestibulum nec
						congueex. Nunc tempor non sem vel aliquet. Proin turpis purus, mollis id est vitae, ultrices
						tinciduntturpis.Nulla acodio placerat, auctor orci in, aliquam magna. Curabitur a nisl sem.
						Maecenas alectus atellus finibus malesuada. Mauris vehicula erat ut neque viverra, in pulvinar
						nibhimperdiet. Maecenas at turpis commodo, viverra leo a, elementum ante. Nam sit amet
						pharetraligula, vel mattis est. Morbi aliquam dolor lorem, in bibendum nunc lacinia vel. In eu
						purus euaugue laoreet tempus. In hac habitasse platea dictumst. Morbi id suscipit purus. Morbi
						lacusaugue, malesuada condimentum ultrices et,mattis eudui. Curabitur at dictum eros, sed
						posuere ex. Fusce laoreet, ex ac viverra finibus, nisi diamfeugiatnulla, quis feugiat diam eros
						vitae tortor.Maecenas vel enim tortor. Quisque eget egestas quam.Quisque augue leo, tempus ac
						urna nec,malesuada vulputate orci. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					</div>
					<div className="column gr-3 gr-12@mobile">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
						Vestibulum nec
						congueex. Nunc tempor non sem vel aliquet. Proin turpis purus, mollis id est vitae, ultrices
						tinciduntturpis.Nulla acodio placerat, auctor orci in, aliquam magna. Curabitur a nisl sem.
						Maecenas alectus atellus finibus malesuada. Mauris vehicula erat ut neque viverra, in pulvinar
						nibhimperdiet. Maecenas at turpis commodo, viverra leo a, elementum ante. Nam sit amet
						pharetraligula, vel mattis est. Morbi aliquam dolor lorem, in bibendum nunc lacinia vel. In eu
						purus euaugue laoreet tempus. In hac habitasse platea dictumst. Morbi id suscipit purus. Morbi
						lacusaugue, malesuada condimentum ultrices et,mattis eudui. Curabitur at dictum eros, sed
						posuere ex. Fusce laoreet, ex ac viverra finibus, nisi diamfeugiatnulla, quis feugiat diam eros
						vitae tortor.Maecenas vel enim tortor. Quisque eget egestas quam.Quisque augue leo, tempus ac
						urna nec,malesuada vulputate orci. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
					</div>
				</div>
				<div className="footer text-center">
					Made by Lervana Katarzyna Dadek
				</div>
			</div>
		);
	}
}
export default Demo;