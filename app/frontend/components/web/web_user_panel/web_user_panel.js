import _ from 'lodash';
import React, { Component } from 'react';
import { translate } from 'react-i18next';
import { observer, inject } from 'mobx-react';

import TasksTable from './web_tasks_table';
import CircularProgress from 'material-ui/CircularProgress';

import '../../../styles/web_user_panel.scss';

@translate( [ 'translation' ], { wait: true } )
@inject( 'baseApiURL', 'lang', 'tasksStore', 'notifier' )
@observer
class UserPanel extends Component {

	apiUrl = undefined;

	constructor( props, context ) {
		super( props, context );
	}

	componentDidMount() {
		this.apiUrl = `${this.props.baseApiURL }/tasks`;
		return this.props.tasksStore.get( this.apiUrl ).catch( ( err )=> {
			console.log( err )
		} );
	}

	static renderProgress() {
		return (
			<div className="progress text-center">
				<CircularProgress size={160} thickness={6}/>
			</div>
		);
	}

	render() {
		return (_.isNil( this.props.tasksStore.tasks ) ? UserPanel.renderProgress() : <TasksTable/>);
	}

}

export default UserPanel;