import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { translate } from 'react-i18next';
import { observable } from 'mobx';
import { observer, inject } from 'mobx-react';

import Paper from 'material-ui/Paper';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import DeleteForeverIcon from 'material-ui/svg-icons/action/delete-forever';

import '../../../styles/web_user_panel.scss';

@translate( [ 'translation' ], { wait: true } )
@inject( 'baseApiURL', 'tasksStore', 'notifier' )
@observer
class DeleteTaskBtn extends Component {

	@observable open = false;
	@observable processing = false;

	handleOpen = ()=> {
		this.open = true;
	};

	handleClose = ()=> {
		this.open = false;
		this.processing = false;
	};

	deleteTask = () => {
		this.processing = true;
		return this.props.tasksStore.delete( `${this.props.baseApiURL }/tasks`, this.props.taskId).then(() => {
			this.handleClose();
			return this.props.notifier.show( this.props.t( 'Task deleted' ) );
		});
	};

	render() {
		const actions = [
			<FlatButton label='Cancel' primary={true} onTouchTap={this.handleClose}/>,
			<FlatButton label='OK' primary={true} keyboardFocused={true} onTouchTap={this.deleteTask}/>
		];

		return (
			<div>
				<FlatButton className={ `action-btn ${this.props.className}` } icon={<DeleteForeverIcon/>}
				            onTouchTap={ this.handleOpen }/>
				<Dialog title={this.props.t( `Delete task #${this.props.taskIndex}` )}
				        actions={ this.processing ? '...' : actions}
				        modal={false}
				        open={this.open}
				        onRequestClose={this.handleClose}
				>
					{ _.isNil( this.props.description ) ? null :
						<div>
							<p>{ this.props.t( 'Description' ) }:</p>
							<Paper className='quotation'>{ this.props.description }</Paper>
						</div>
					}
					<p className='text-justify'>{ this.props.t( 'Are you sure that you want to delete this task? This action cannot be undone.' ) }</p>
				</Dialog>
			</div>
		);
	}
}

DeleteTaskBtn.propTypes = {
	taskId: PropTypes.number.isRequired,
	taskIndex: PropTypes.number.isRequired,
	description: PropTypes.string,
	className: PropTypes.string
};

export default DeleteTaskBtn;