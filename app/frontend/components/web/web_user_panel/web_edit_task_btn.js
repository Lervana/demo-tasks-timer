import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { translate } from 'react-i18next';
import { observable } from 'mobx';
import { observer, inject } from 'mobx-react';

import Paper from 'material-ui/Paper';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import ModeEditIcon from 'material-ui/svg-icons/editor/mode-edit';

import '../../../styles/web_user_panel.scss';

@translate( [ 'translation' ], { wait: true } )
@inject( 'baseApiURL', 'tasksStore', 'notifier' )
@observer
class DeleteTaskBtn extends Component {

	@observable open = false;
	@observable newDescription = undefined;
	@observable processing = false;

	componentDidMount() {
		this.newDescription = this.props.description;
	}

	handleOpen = ()=> {
		this.open = true;
	};

	handleClose = ()=> {
		this.open = false;
		this.processing = false;
	};

	editTask = ()=> {
		this.processing = true;
		return this.props.tasksStore.update( `${this.props.baseApiURL }/tasks`, this.props.taskId, this.newDescription).then(() => {
			this.handleClose();
			return this.props.notifier.show( this.props.t( 'Task updated' ) );
		});
	};

	render() {
		const actions = [
			<FlatButton label="Cancel" primary={true} onTouchTap={this.handleClose}/>,
			<FlatButton label="OK" primary={true} keyboardFocused={true} onTouchTap={this.editTask }/>
		];

		return (
			<div>
				<FlatButton className={ `action-btn ${this.props.className}` } icon={<ModeEditIcon/>}
				            onTouchTap={ this.handleOpen }/>
					<Dialog title={this.props.t( `Edit task #${this.props.taskIndex}` )}
					        actions={ this.processing ? '...' : actions}
					        modal={false}
					        open={this.open}
					        onRequestClose={this.handleClose}
					>
						{ _.isNil( this.props.description ) ? null :
							<div>
								<p>{ this.props.t( 'Description' ) }:</p>
								<Paper className='quotation'>{ this.props.description }</Paper>
							</div>
						}
						<div>
							<p>{ this.props.t( 'New description' ) }:</p>
							<Paper className="edit-description">
								<TextField name="description"
								           className='editor'
								           floatingLabelFixed={ true }
								           floatingLabelText={ `${ 150 - _.get( this.newDescription, 'length', 0 ) } ${this.props.t( 'chars left' )}`}
								           value={ this.newDescription || '' }
								           onChange={( e )=> {
									           this.newDescription = e.target.value.substr( 0, 150 );
								           }}
								           multiLine={ true }
								           rowsMax={ 5 }
								/>
							</Paper>
						</div>
					</Dialog>
			</div>
		);
	}

}

DeleteTaskBtn.propTypes = {
	taskId: PropTypes.number.isRequired,
	taskIndex: PropTypes.number.isRequired,
	description: PropTypes.string,
	className: PropTypes.string
};

export default DeleteTaskBtn;