import _ from 'lodash';
import moment from 'moment';
import React, { Component } from 'react';
import { translate } from 'react-i18next';
import { observer, inject } from 'mobx-react';

import EditTaskBtn from './web_edit_task_btn';
import DeleteTaskBtn from './web_delete_task_btn';
import FlatButton from 'material-ui/FlatButton';
import RefreshIcon from 'material-ui/svg-icons/navigation/refresh';

import '../../../styles/web_user_panel.scss';

@translate( [ 'translation' ], { wait: true } )
@inject( 'lang', 'tasksStore', 'notifier', 'baseApiURL' )
@observer
class TasksTable extends Component {

	showMessage = ( message )=> {
		this.props.notifier.show( message );
	};

	convertTasks = ()=> {
		let tasks = [];

		_.forEach( this.props.tasksStore.getSortedTasks(), ( task, index )=> {
			let start = moment( task.start ).format( 'YYYY-MM-DD HH:mm:ss' );
			let stop = moment( task.stop ).format( 'YYYY-MM-DD HH:mm:ss' );
			let duration = moment.duration( task.duration, 'seconds' );
			let lang = this.props.lang.code;
			let durationText = duration.locale( lang ).humanize();
			tasks.push( this.renderRow( task.id, index + 1, task.description, start, stop, durationText ) )
		} );

		return tasks;
	};

	refreshButtonAction = ()=> {
		this.props.tasksStore.refresh( `${this.props.baseApiURL }/tasks` );
		this.showMessage( this.props.t( 'Tasks refreshed' ) );
	};

	renderRefreshBtn( className ) {
		return <div className={ className }>
			<FlatButton className={ 'action-btn' } icon={<RefreshIcon/>} onTouchTap={ this.refreshButtonAction }/>
		</div>
	}

	renderRow( id, index, description, start, stop, duration, isHeader = false ) {
		return (
			<div key={ index } className={ `row row-align-middle ${ isHeader ? 'header hide@mobile' : '' }` }>
				<div className="gr-1 hide@mobile text-center">{ index }</div>
				<div className="gr-5 gr-12@mobile row-align-middle">
					<div className="gr-10 gr-12@mobile text-justify crop">{ description }</div>
					<div className="hide show@mobile gr-12@mobile" style={{ height: '1rem' }}/>
					<div className="gr-2 hide@mobile text-right">{ isHeader ? null :
						<EditTaskBtn taskId={ id } taskIndex={ index } description={ description }/> }</div>
				</div>
				<div className="gr-2 gr-12@mobile text-center">{ start }</div>
				<div className="gr-2 gr-12@mobile text-center">{ stop }</div>
				<div className="gr-1 gr-12@mobile text-center">{ duration }</div>
				<div className="gr-1 hide@mobile text-center ">{ isHeader ? this.renderRefreshBtn() :
					<DeleteTaskBtn taskId={ id } taskIndex={ index } description={ description }/> }</div>
				{ isHeader ? null :
					<div className="gr-12 hide show@mobile ">
						<EditTaskBtn className='float-right' taskId={ id } taskIndex={ index }
						             description={ description }/>
						<DeleteTaskBtn className='float-left' taskId={ id } taskIndex={ index }
						               description={ description }/>
					</div>
				}
			</div>
		);
	}

	render() {
		return (<div className="tasks-table row">
			{ this.renderRefreshBtn( 'gr-12 hide show@mobile text-center' )}
			{ this.renderRow( null, '#', this.props.t( 'Description' ), this.props.t( 'Start' ), this.props.t( 'Stop' ), this.props.t( 'Duration' ), true ) }
			{ this.convertTasks() }
		</div>);
	}
}

export default TasksTable;