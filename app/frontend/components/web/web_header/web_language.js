import i18n from 'i18next';
import cookie from 'react-cookie';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import { translate } from 'react-i18next';

import FlatButton from 'material-ui/FlatButton';

@translate( [ 'translation' ], { wait: true } )
@inject( 'lang' )
@observer
class Language extends Component {

	constructor( props, context ) {
		super( props, context );
		this.props.lang.set( cookie.load( 'demo-tasks-timer-lang' ) );
	}

	setLang = ( code )=> {
		cookie.save( 'demo-tasks-timer-lang', code );
		i18n.changeLanguage( code );
		this.props.lang.set( code );
	};

	renderLanguageButton( code ) {
		return (
			<FlatButton className='web-language-button'
			            label={ code.substr( 3, 2 ) }
			            onTouchTap={ this.setLang.bind( this, code ) }
			/>
		);
	}

	render() {
		return (
			<div className={ this.props.className }>
				{this.renderLanguageButton( 'pl-PL' )}
				{this.renderLanguageButton( 'en-GB' )}
			</div>
		);
	}
}

Language.propTypes = {
	className: PropTypes.string
};

export default Language;