import _ from 'lodash';
import cookie from 'react-cookie';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { observable } from 'mobx';
import { translate } from 'react-i18next';
import { Link, Redirect } from 'react-router-dom';
import { observer, inject } from 'mobx-react';

import RaisedButton from 'material-ui/RaisedButton';

@translate( [ 'translation' ], { wait: true } )
@inject( 'user', 'timer' )
@observer
class Menu extends Component {

	@observable redirect = undefined;

	constructor( props, context ) {
		super( props, context );
		let userCookie = cookie.load( 'demo-tasks-timer-user' );
		if( !_.isNil( userCookie ) && userCookie ) this.props.user.login();
		else this.props.user.logout();
	}

	login = ()=> {
		cookie.save( 'demo-tasks-timer-user', true );
		this.props.user.login();
		this.redirect = '/user';
	};

	logout = ()=> {
		cookie.remove( 'demo-tasks-timer-user' );
		this.props.timer.reset();
		this.props.user.logout();
		this.redirect = '/';
	};

	componentDidUpdate() {
		this.redirect = undefined;
	}

	renderPublicMenu() {
		return (
			<div className='gr-12'>
				<RaisedButton className='menu-button'
				              label={ this.props.t( 'Login' ) }
				              secondary={ true }
				              onTouchTap={ this.login }/>
			</div>
		);
	}

	renderPrivateMenu() {
		return (
			<div className='gr-12'>
				<Link to='/'><RaisedButton className='menu-button ' label={ this.props.t( 'Home' ) } primary={ true }/></Link>
				<Link to='/user'><RaisedButton className='menu-button' label={ this.props.t( 'Tasks list' ) }
				                               primary={ true }/></Link>
				<RaisedButton className='menu-button' label={ this.props.t( 'Logout' ) } secondary={ true }
				              onTouchTap={ this.logout }/>
			</div>
		);
	}

	render() {
		if( !_.isNil( this.redirect ) ) return <Redirect to={ this.redirect }/>;

		return (
			<div className={ this.props.className }>
				{ this.props.user.isLogged ? this.renderPrivateMenu() : this.renderPublicMenu() }
			</div>
		);
	}
}

Menu.propTypes = {
	className: PropTypes.string
};

export default Menu;