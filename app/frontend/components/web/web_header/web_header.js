import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { translate } from 'react-i18next';

import WebMenu from './web_menu';
import Language from './web_language';
import CurrentTask from './web_current_task';

import '../../../styles/web_header.scss';

@translate( [ 'translation' ], { wait: true } )
@observer
class Header extends Component {

	render() {
		return (
			<div className="header row row-full row-align-middle row-align-right">
				<div className="gr-12">
					<Language className="gr-4 gr-12@mobile text-left"/>
					<WebMenu className="gr-8 row@mobile row-full@mobile text-right"/>
				</div>
				<CurrentTask className='row row-full'/>
			</div>
		);
	}
}

export default Header;