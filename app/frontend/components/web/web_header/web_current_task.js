import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { observable } from 'mobx';
import { translate } from 'react-i18next';
import { observer, inject } from 'mobx-react';

import Divider from 'material-ui/Divider';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';

import 'whatwg-fetch';

@translate( [ 'translation' ], { wait: true } )
@inject( 'user', 'timer', 'baseApiURL', 'notifier', 'tasksStore' )
@observer
class Timer extends Component {

	@observable description = undefined;
	@observable processing = false;
	@observable message = undefined;

	componentWillUnmount() {
		this.props.timer.reset();
	}

	showMessage = ( message )=> {
		this.props.notifier.show( message );
	};

	stopTimer = ()=> {
		this.processing = true;
		this.props.timer.stop();

		return this.props.tasksStore.add(`${this.props.baseApiURL }/tasks`, this.description, this.props.timer.startTime, this.props.timer.stopTime).then( ()=> {
			this.props.timer.reset();
			this.processing = false;
			this.description = undefined;

			this.showMessage( this.props.t( 'Tasks added' ) );
			return undefined;
		} ).catch( ( ex )=> {
			console.log( ex );
		} );
	};

	renderClock() {
		let duration = this.props.timer.formattedDuration;

		return (
			<div className="timer gr-12 gr-centered text-center">
				<div className="timer-number">
					<div className="timer-text">{ Math.round( duration.asHours(), 0 ) }</div>
				</div>
				<label>:</label>
				<div className="timer-number">
					<div className="timer-text">{ duration.minutes() }</div>
				</div>
				<label>:</label>
				<div className="timer-number">
					<div className="timer-text">{ duration.seconds() }</div>
				</div>
			</div>
		);
	}

	renderBtn() {
		if( this.processing ) return <CircularProgress size={ 40 } className='action-button'/>;
		let label = this.props.timer.started ? this.props.t( 'stop' ) : this.props.t( 'start' );
		let action = this.props.timer.started ? this.stopTimer : this.props.timer.start;
		return <RaisedButton label={ label } primary={ true } className='action-button' onTouchTap={ action }/>
	}



	render() {
		if( !this.props.user.isLogged ) return ( null );

		return (
			<div className={ `${ this.props.className } task-form` }>
				<Divider/>
				<TextField name="description"
				           className='text-field gr-centered'
				           floatingLabelFixed={ true }
				           floatingLabelText={ `${this.props.t( 'Description' )} ( ${ 150 - _.get( this.description, 'length', 0 ) } ${this.props.t( 'chars left' )} )`}
				           hintText='New task'
				           value={ this.description || '' }
				           onChange={( e )=> {
				           	    this.description = e.target.value.substr( 0, 150 );
				           }}
				/>
				{ this.renderClock() }
				<div className="gr-2 gr-centered gr-12@mobile text-center btns-container">
					{ this.renderBtn() }
				</div>
			</div>
		);
	}

}

Timer.propTypes = {
	className: PropTypes.string
};

export default Timer;