import { observable } from 'mobx';

class User {

	@observable isLogged = false;

	login = ()=> {
		this.isLogged = true;
	};

	logout = ()=> {
		this.isLogged = false;
	};
}

export default User;