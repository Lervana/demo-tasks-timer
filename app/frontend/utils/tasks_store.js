import _ from 'lodash';
import { observable } from 'mobx';
import 'whatwg-fetch';

class TasksStore {

	@observable tasks = undefined;

	getSortedTasks = ()=> {
		return _.orderBy( this.tasks, [ 'stop' ], [ 'desc' ] );
	};

	get = ( apiUrl )=> {
		return fetch( apiUrl ).then( ( response )=> {
			return response.json()
		} ).then( ( json )=> {
			if( _.has( json, 'extra.tasks' ) ) this.tasks = json.extra.tasks;
			return this.tasks;
		} );
	};

	add = ( apiUrl, description, start, stop )=> {

		let body = {
			description,
			start,
			stop
		};

		return fetch( apiUrl, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify( body )
		} ).then( ( response )=> {
			return response.json()
		} ).then( ( json )=> {
			if( _.has( json, 'code' ) && json.code === 0 ) {
				if( _.isNil( this.tasks ) ) this.tasks = [];
				if( _.has( json, 'extra.task' ) ) this.tasks.push( json.extra.task );
			}
			else throw new Error( json.message );
		} );
	};

	update = ( apiUrl, id, description )=> {

		let body = { id, description };

		return fetch( apiUrl, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify( body )
		} ).then( ( response )=> {
			return response.json()
		} ).then( ( json )=> {
			if( _.has( json, 'code' ) && json.code === 0 ) {
				let index = _.findIndex( this.tasks, ( task )=> {
					return task.id === json.extra.id;
				} );
				this.tasks[ index ].description = json.extra.new_description;
			}
			else throw new Error( json.message );
		} );
	};

	delete = ( apiUrl, id )=> {

		let body = { id };

		return fetch( apiUrl, {
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify( body )
		} ).then( ( response )=> {
			return response.json()
		} ).then( ( json )=> {
			if( _.has( json, 'code' ) && json.code === 0 ) {
				return _.remove( this.tasks, ( task )=> {
					return task.id === id;
				} );
			}
			else throw new Error( json.message );
		} );
	};

	refresh = ( apiUrl )=> {
		this.get( apiUrl );
	}
}

export default TasksStore;