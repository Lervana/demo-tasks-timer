import { observable } from 'mobx';

class Notifier {
	@observable open = false;
	@observable message = '';

	show = ( message ) => {
		this.open = true;
		this.message = message;
	};

	close = () => {
		this.open = false;
		this.message = '';
	};
}

export default Notifier;