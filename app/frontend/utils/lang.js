import { observable } from 'mobx';

class Lang {

	@observable code = false;

	set = ( code )=> {
		this.code = code;
	};
}

export default Lang;