import i18n from 'i18next';
import XHR from 'i18next-xhr-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

function loadLocales( url, options, callback ) {
	try {
		let waitForLocale = require( 'bundle-loader!./../locales/converted' + url );

		waitForLocale( ( locale )=> {
			callback( locale, { status: '200' } );
		} )
	} catch( e ) {
		callback( null, { status: '404' } );
	}
}

i18n
	.use( XHR )
	.use( LanguageDetector )
	.init( {
		fallbackLng: 'en',
		debug: false,//process.env.NODE_ENV === 'development',
		backend: {
			loadPath: '/{{lng}}.json',
			parse: ( data )=>data,
			ajax: loadLocales
		},
		detection: {
			order: [ 'cookie', 'localStorage', 'navigator' ],
			lookupCookie: 'demo-tasks-timer-lang',
			lookupLocalStorage: 'demo-tasks-timer-lang',
			caches: [ 'localStorage', 'cookie' ]
		}
	} );

export default i18n;
