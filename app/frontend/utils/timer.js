import _ from 'lodash';
import moment from 'moment';
import { observable, computed } from 'mobx';

class Timer {
	@observable duration = 0;
	@observable started = false;
	@observable startTime = undefined;

	stopwatch = undefined;

	@computed get formattedDuration() {
		return moment.duration( this.duration, 'seconds' );
	}

	@computed get stopTime() {
		return moment( this.startTime ).add( this.duration, 's' )
	}

	start = ()=> {
		this.reset();
		this.stopwatch = setInterval( ()=>
				this.duration = this.duration + 1
			, 1000 );
		this.startTime = moment();
		this.started = true;
	};

	stop = () => {
		if( !_.isNil( this.stopwatch ) ) clearInterval( this.stopwatch );
	};

	reset = ()=> {
		this.duration = 0;
		this.started = false;
		this.stop();
	};
}

export default Timer;