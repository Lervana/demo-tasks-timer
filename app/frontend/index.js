import React from 'react';
import ReactDOM from 'react-dom';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import injectTapEventPlugin from 'react-tap-event-plugin';

import { Provider } from 'mobx-react';
import { MuiThemeProvider } from 'material-ui';
import { I18nextProvider } from 'react-i18next';
import { BrowserRouter, Route, Redirect } from 'react-router-dom';

import Web from './components/web/web';
import PublicView from './components/web/web_demo';
import UserTasks from './components/web/web_user_panel/web_user_panel';

import i18n from './utils/i18n';
import User from './utils/user';
import Lang from './utils/lang';
import Timer from './utils/timer';
import Notifier from './utils/notifier';
import TasksStore from './utils/tasks_store';

injectTapEventPlugin();

let muiTheme = getMuiTheme( {
	palette: {
		primary1Color: '#324D5B',
		primary2Color: '#00131F',
		primary3Color: '#9AB0BD',
		accent1Color: '#ff4081',
		alternateTextColor: '#ffffff'
	}
} );

let user = new User();
let lang = new Lang();
let timer = new Timer();
let tasksStore = new TasksStore();
let notifier = new Notifier();

const PrivateRoute = ({ component: Component, ...rest }) => (
	<Route {...rest} render={props => (
		user.isLogged ? (
			<Component {...props}/>
		) : (
			<Redirect to={{
				pathname: '/',
				state: { from: props.location }
			}}/>
		)
	)}/>
);

ReactDOM.render( (
	<I18nextProvider i18n={ i18n }>
		<Provider
			baseURL={`${process.env.BASE_URL}`}
			baseApiURL={`${process.env.BASE_URL}/api`}
		    user={ user }
		    timer={ timer }
		    lang={ lang }
			notifier={ notifier }
			tasksStore={ tasksStore }
		>
			<MuiThemeProvider muiTheme={ muiTheme }>
				<BrowserRouter>
					<Web>
						<Route exact path="/" component={PublicView}/>
						<PrivateRoute path="/user" component={UserTasks}/>
					</Web>
				</BrowserRouter>
			</MuiThemeProvider>
		</Provider>
	</I18nextProvider>
), document.getElementById( 'root' ) );