import _ from 'lodash';
import chai from 'chai';
import crypto from 'crypto';
import moment from 'moment';
import chaiAsPromised from 'chai-as-promised';
import { errorCodes, success } from '../../backend/common/error';
import { sendApiRequest } from '../tests_utils/api_helpers';
import { verifyErrorRedirect, verifyMessageRedirect } from '../tests_utils/validation_helpers';
import { databaseReset, databaseCreateTask, databaseGetTask } from '../tests_utils/database_helpers';
import { messageCodes } from './../../backend/routes/helpers';

chai.use( chaiAsPromised );
chai.Should();

let start = moment();
let stop = moment( start ).add( 10, 'm' );
let duration = stop.diff( start, 's' );

let defaultTask = {
	description: 'Default task',
	start,
	stop,
	duration
};

let newDescription = 'Updated task';
let taskId = undefined;

describe( "api:task:update", ()=> {

	before( ()=> {
		return databaseReset().then( ()=> {
			return databaseCreateTask( defaultTask.description, defaultTask.start, defaultTask.stop, defaultTask.duration ).then( ( task )=> {
				taskId = task.id;
			} );
		} );
	} );

	it( "description cannot have more than 150 chars", ()=> {
		let data = {
			id: taskId,
			description: crypto.randomBytes( 77 ).toString( 'hex' )
		};

		return sendApiRequest( 'PUT', '/tasks', data, null ).then( ( res )=> {
			let expected = {
				name: 'invalid-parameter',
				message: '"description" length must be less than or equal to 150 characters long',
				code: errorCodes.InvalidParameter
			};
			res.should.be.eql( expected );
		} );
	} );

	it( "id is required", ()=> {
		let data = { description: newDescription };

		return sendApiRequest( 'PUT', '/tasks', data, null ).then( ( res )=> {
			let expected = {
				name: 'invalid-parameter',
				message: '"id" is required',
				code: errorCodes.InvalidParameter
			};
			res.should.be.eql( expected );
		} );
	} );

	it( "description is required", ()=> {
		let data = { id: taskId };

		return sendApiRequest( 'PUT', '/tasks', data, null ).then( ( res )=> {
			let expected = {
				name: 'invalid-parameter',
				message: '"description" is required',
				code: errorCodes.InvalidParameter
			};
			res.should.be.eql( expected );
		} );
	} );

	it( "can update task", ()=> {
		return databaseGetTask( taskId ).then( ( task )=> {
			task.description.should.be.eql( defaultTask.description );
			moment( task.start ).toString().should.be.eql( moment( defaultTask.start ).toString() );
			moment( task.stop ).toString().should.be.eql( moment( defaultTask.stop ).toString() );
			task.duration.should.be.eql( defaultTask.stop.diff( defaultTask.start, 's' ) );

			let data = {
				id: taskId,
				description: newDescription
			};

			return sendApiRequest( 'PUT', '/tasks', data, null ).then( ( res )=> {
				let expected = {
					id: taskId,
					old_description: defaultTask.description,
					new_description: newDescription
				};
				res.should.be.eql( success( expected ) );
				return databaseGetTask( taskId );
			} ).then( ( updatedTask )=> {
				updatedTask.description.should.be.eql( newDescription );
				moment( updatedTask.start ).toString().should.be.eql( moment( defaultTask.start ).toString() );
				moment( updatedTask.stop ).toString().should.be.eql( moment( defaultTask.stop ).toString() );
				updatedTask.duration.should.be.eql( defaultTask.stop.diff( defaultTask.start, 's' ) );
				return undefined;
			} );
		} );
	} );

	it( "can update task to empty description", ()=> {
		return databaseGetTask( taskId ).then( ( task )=> {
			task.description.should.be.eql( newDescription );
			moment( task.start ).toString().should.be.eql( moment( defaultTask.start ).toString() );
			moment( task.stop ).toString().should.be.eql( moment( defaultTask.stop ).toString() );
			task.duration.should.be.eql( defaultTask.stop.diff( defaultTask.start, 's' ) );

			let data = {
				id: taskId,
				description: null
			};

			return sendApiRequest( 'PUT', '/tasks', data, null ).then( ( res )=> {
				let expected = {
					id: taskId,
					old_description: newDescription,
					new_description: null
				};
				res.should.be.eql( success( expected ) );
				return databaseGetTask( taskId );
			} ).then( ( updatedTask )=> {
				(updatedTask.description === null).should.be.eql( true );
				moment( updatedTask.start ).toString().should.be.eql( moment( defaultTask.start ).toString() );
				moment( updatedTask.stop ).toString().should.be.eql( moment( defaultTask.stop ).toString() );
				updatedTask.duration.should.be.eql( defaultTask.stop.diff( defaultTask.start, 's' ) );
				return undefined;
			} );
		} );
	} );

} );