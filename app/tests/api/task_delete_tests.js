import chai from 'chai';
import moment from 'moment';
import chaiAsPromised from 'chai-as-promised';
import { errorCodes, success } from '../../backend/common/error';
import { expect } from 'chai';
import { sendApiRequest } from '../tests_utils/api_helpers';
import { databaseReset, databaseCreateTask, databaseGetTask } from '../tests_utils/database_helpers';
import { messageCodes } from './../../backend/routes/helpers';

chai.use( chaiAsPromised );
chai.Should();

let start = moment();
let stop = moment( start ).add( 10, 'm' );
let duration = stop.diff( start, 's' );

let defaultTask = {
	description: 'Default task',
	start,
	stop,
	duration
};

let testTaskId = undefined;

describe( "api:task:delete", ()=> {

	before( ()=> {
		return databaseReset().then( ()=> {
			return databaseCreateTask( defaultTask.description, defaultTask.start, defaultTask.stop, defaultTask.duration ).then( ( task )=> {
				testTaskId = task.id;
			} );
		} );
	} );

	it( "id is required", ()=> {
		let data = {};

		return sendApiRequest( 'DELETE', '/tasks', data, null ).then( ( res )=> {
			let expected = {
				name: 'invalid-parameter',
				message: '"id" is required',
				code: errorCodes.InvalidParameter
			};
			res.should.be.eql( expected );
		} );
	} );

	it( "can delete task", ()=> {
		return sendApiRequest( 'DELETE', '/tasks', { id: testTaskId }, null ).then( ( res )=> {
			res.should.be.eql( success() );
		} ).then( ()=> {
			return databaseGetTask( testTaskId ).then( ()=> {
				return expect( ()=> {
				} ).to.throw( Error );
			} ).catch( ( err )=> {
				return err.should.be.eql( {
					name: 'task-not-found',
					message: `Task not found for id ${ testTaskId }`,
					code: -101
				} );
			} );
		} );
	} );
} );