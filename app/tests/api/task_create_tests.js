import _ from 'lodash';
import chai from 'chai';
import crypto from 'crypto';
import moment from 'moment';
import chaiAsPromised from 'chai-as-promised';
import { errorCodes, success } from '../../backend/common/error';
import { sendApiRequest } from '../tests_utils/api_helpers';
import { databaseReset, databaseCreateTask, databaseGetTaskByDescription, databaseGetTask } from '../tests_utils/database_helpers';
import { messageCodes } from './../../backend/routes/helpers';

chai.use( chaiAsPromised );
chai.Should();

let start = moment();
let stop = moment( start ).add( 10, 'm' );
let duration = stop.diff( start, 's' );

let defaultTask = {
	description: 'Default task',
	start,
	stop,
	duration
};

let validRequestData = {
	description: 'New test task',
	start: moment( start ).add( 1, 'h' ),
	stop: moment( start ).add( 2, 'h' )
};

describe( "api:task:create", ()=> {

	before( ()=> {
		return databaseReset().then( ()=> {
			return databaseCreateTask( defaultTask.description, defaultTask.start, defaultTask.stop, defaultTask.duration );
		} );
	} );

	it( "description cannot have more than 150 chars", ()=> {
		let data = {
			...validRequestData,
			description: crypto.randomBytes( 77 ).toString( 'hex' )
		};

		return sendApiRequest( 'POST', '/tasks', data, null ).then( ( res )=> {
			let expected = {
				name: 'invalid-parameter',
				message: '"description" length must be less than or equal to 150 characters long',
				code: errorCodes.InvalidParameter
			};
			res.should.be.eql( expected );
		} );
	} );

	it( "start is required", ()=> {
		let data = _.omit( validRequestData, 'start' );

		return sendApiRequest( 'POST', '/tasks', data, null ).then( ( res )=> {
			let expected = {
				name: 'invalid-parameter',
				message: '"start" is required',
				code: errorCodes.InvalidParameter
			};
			res.should.be.eql( expected );
		} );
	} );

	it( "stop is required", ()=> {
		let data = _.omit( validRequestData, 'stop' );

		return sendApiRequest( 'POST', '/tasks', data, null ).then( ( res )=> {
			let expected = {
				name: 'invalid-parameter',
				message: '"stop" is required',
				code: errorCodes.InvalidParameter
			};
			res.should.be.eql( expected );
		} );
	} );

	it( "stop cannot be before start", ()=> {
		let data = {
			start: moment( start ).add( 10, 'd' ),
			stop: moment( start ).add( 1, 'h' )
		};

		return sendApiRequest( 'POST', '/tasks', data, null ).then( ( res )=> {
			let expected = {
				name: 'invalid-parameter',
				message: 'Stop cannot be before start',
				code: errorCodes.InvalidParameter
			};
			res.should.be.eql( expected );
		} );
	} );

	it( "can add task", ()=> {
		return sendApiRequest( 'POST', '/tasks', validRequestData, null ).then( ( res )=> {
			let expected = success();
			res.code.should.be.eql( expected.code );
			res.message.should.be.eql( expected.message );
			res.name.should.be.eql( expected.name );
		} ).then( ()=> {
			return databaseGetTaskByDescription( validRequestData.description );
		} ).then( ( task )=> {
			task.description.should.be.eql( validRequestData.description );
			moment( task.start ).toString().should.be.eql( moment( validRequestData.start ).toString() );
			moment( task.stop ).toString().should.be.eql( moment( validRequestData.stop ).toString() );
			task.duration.should.be.eql( validRequestData.stop.diff( validRequestData.start, 's' ) );
			return undefined;
		} );
	} );

	it( "can add task without description", ()=> {
		let data = _.omit( validRequestData, 'description' );

		return sendApiRequest( 'POST', '/tasks', data, null ).then( ( res )=> {
			let expected = success();
			res.code.should.be.eql( expected.code );
			res.message.should.be.eql( expected.message );
			res.name.should.be.eql( expected.name );
			return res.extra.task.id;
		} ).then( ( id )=> {
			return databaseGetTask( id );
		} ).then( ( task )=> {
			(task.description === null).should.be.eql( true );
			moment( task.start ).toString().should.be.eql( moment( data.start ).toString() );
			moment( task.stop ).toString().should.be.eql( moment( data.stop ).toString() );
			task.duration.should.be.eql( data.stop.diff( data.start, 's' ) );
			return undefined;
		} );
	} );
} );