import chai from 'chai';
import moment from 'moment';
import chaiAsPromised from 'chai-as-promised';
import { sendApiRequest } from '../tests_utils/api_helpers';
import { success } from '../../backend/common/error';
import { databaseReset, databaseCreateTask } from '../tests_utils/database_helpers';

chai.use( chaiAsPromised );
chai.Should();

let start1 = moment().add( 15, 'm' );
let stop1 = moment().add( 25, 'm' );
let start2 = moment().add( 20, 'm' );
let stop2 = moment().add( 40, 'm' );


let task1 = {
	description: 'Task1',
	start: start1,
	stop: stop1,
	duration: stop1.diff( start1, 's' )
};

let task2 = {
	description: 'Task2',
	start: start2,
	stop: stop2,
	duration: stop2.diff( start2, 's' )
};

describe( "api:task:get", ()=> {

	before( ()=> {
		return databaseReset().then( ()=> {
			return databaseCreateTask( task1.description, task1.start, task1.stop, task1.duration ).then( ( createdTask1 )=> {
				task1.id = createdTask1.id;
				task1.duration = createdTask1.duration;
				return databaseCreateTask( task2.description, task2.start, task2.stop, task2.duration ).then( ( createdTask2 )=> {
					task2.id = createdTask2.id;
					task2.duration = createdTask2.duration;
				} );
			} );
		} );
	} );

	it( "can get tasks", ()=> {
		return sendApiRequest( 'GET', '/tasks' ).then( ( res )=> {

			let expected = success();
			res.code.should.be.eql( expected.code );
			res.message.should.be.eql( expected.message );
			res.name.should.be.eql( expected.name );

			let tasks = res.extra.tasks;

			tasks[0].id.should.be.eql(task1.id);
			moment( tasks[0].start ).toString().should.be.eql( moment( task1.start ).toString() );
			moment( tasks[0].stop ).toString().should.be.eql( moment( task1.stop ).toString() );
			tasks[0].duration.should.be.eql(task1.duration);

			tasks[1].id.should.be.eql(task2.id);
			moment( tasks[1].start ).toString().should.be.eql( moment( task2.start ).toString() );
			moment( tasks[1].stop ).toString().should.be.eql( moment( task2.stop ).toString() );
			tasks[1].duration.should.be.eql(task2.duration);

			return undefined;
		} );
	} );
} );