import { sequelize, Task } from './../../backend/db_models';

export let databaseReset = ()=> {
	if( process.env.NODE_ENV === 'test' ) {
		return sequelize.query( 'DELETE FROM tasks' );
	} else {
		throw new Error( "Wrong environment!" );
	}
};

export let databaseCreateTask = ( description, start, stop, duration )=> {
	return Task.create( { description, start, stop, duration } );
};

export let databaseGetTask = ( id )=> {
	return Task.getById( id );
};

export let databaseGetTaskByDescription = ( description )=> {
	return Task.findOne( { where: { description: { $iLike: description } } } );
};