import url from 'url';
import chai from 'chai';
import config from 'config';
import BbPromise from 'bluebird';
import chaiAsPromised from 'chai-as-promised';

chai.use( chaiAsPromised );
chai.Should();

let defaultRedirectUrl = `${config.app.base_url}/user`;

export let verifyErrorRedirect = ( res, code, redirectUrl = defaultRedirectUrl )=> {
	res.statusCode.should.eql( 302 );
	let redirect = url.parse( redirectUrl, true );
	redirect.query[ 'error_code' ] = code;
	res.headers.location.should.eql( url.format( redirect ) );
	return BbPromise.resolve();
};

export let verifyMessageRedirect = ( res, code, redirectUrl = defaultRedirectUrl )=> {
	res.statusCode.should.eql( 302 );
	let redirect = url.parse( redirectUrl, true );
	redirect.query[ 'message_code' ] = code;
	res.headers.location.should.eql( url.format( redirect ) );
	return BbPromise.resolve();
};
