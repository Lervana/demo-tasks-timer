import _ from 'lodash';
import BbPromise from 'bluebird';
import request from 'request';
import config from 'config';

let requestAsync = BbPromise.promisify( request );

export let sendRequest = ( params )=> {
	return new BbPromise( ( resolve, reject )=> {
		let execute = ()=> {
			requestAsync( params ).then( ( res )=> {
				return ( res.statusCode === 200 ) ? resolve( res.body ) : reject( res );
			}, ( err )=> {
				if( err.code === 'ECONNREFUSED' && config.allow_requests_retry ) setTimeout( execute, 1000 );
				else reject( err );
			} );
		};
		execute();
	} );
};

export let sendApiRequest = ( method, uri, data, token, extraParams )=> {
	let params = {
		url: `${ config.app.api_url }${ uri }`,
		headers: { Accept: 'application/json' },
		json: true,
		method: method.toUpperCase(),
		...extraParams
	};

	if( !_.isNil( data ) ) params.json = data;
	if( !_.isNil( token ) ) params.headers.Authorization = `Bearer ${token}`;
	return sendRequest( params );
};
