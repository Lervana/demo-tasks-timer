#!/bin/bash
cd -P "$( dirname "${BASH_SOURCE[0]}" )"

export NODE_ENV=test
export NODE_CONFIG_DIR=./../app/_dist/_config
export LOG_LEVEL=ERROR
./node_modules/.bin/nodemon -w _dist -d 0.5 -x ./node_modules/.bin/mocha _dist/tests/**/*.js -r should -t 10000
