var del = require( 'del' );
var gulp = require( 'gulp' );
var babel = require( 'gulp-babel' );
var flatten = require( 'gulp-flatten' );
var plumber = require( 'gulp-plumber' );
var po2json = require( 'gulp-po2json' );
var sourcemaps = require( 'gulp-sourcemaps' );

function make() {
	return gulp.src( [ '{backend,_config,tests}/**/*.js' ] )
		.pipe( plumber() )
		.pipe( sourcemaps.init() )
		.pipe( babel( {} ) )
		.pipe( sourcemaps.write( '.', {
			sourceRoot: ''
		} ) )
		.pipe( gulp.dest( '_dist' ) );
}

gulp.task( 'dist', [], function() {
	return make();
} );

gulp.task( 'watch', [ 'default' ], function() {
	gulp.watch( [ '{backend,_config,tests}/**/*.js' ], [ 'dist' ] );
} );

gulp.task( 'pot', function() {
	return gulp.src( [ 'frontend/**/*.js' ] )
		.pipe( plumber() )
		.pipe( babel( {
			plugins: [
				[ "babel-gettext-plugin", {
					functionNames: {
						"t": [ 'msgid' ],
						"nt": [ 'msgid', 'msgid_plural', 'count' ],
						"tCtx": [ 'msgctxt', 'msgid' ]
					},
					fileName: 'frontend/locales/raw/template.pot'
				} ]
			]
		} ) );
} );

gulp.task( 'po2json', function() {
	return gulp.src( [ 'frontend/**/*.po' ] )
		.pipe( po2json( { format: "mf" } ) )
		.pipe( flatten( { includeParents: 0 } ) )
		.pipe( gulp.dest( 'frontend/locales/converted' ) );
} );

gulp.task( 'default', function() {
	return del( [ '_dist/**' ] ).then( ()=> {
		return make();
	} );
} );
