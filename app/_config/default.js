import _ from 'lodash';

if( _.isNil( process.env.NO_ENVFILE ) ) {
	let dotenv = require( 'dotenv' );
	if( process.env.NODE_ENV == 'test' ) dotenv.load( { path: ".env.test" } );
	dotenv.load( { path: ".env" } );
}

module.exports = {

	app: {
		log_level: process.env.LOG_LEVEL || 'ERROR',
		port: process.env.SERVER_PORT,
		name: 'demo-tasks-timer',
		api_url: `${process.env.BASE_URL}/api`,
		base_url: process.env.BASE_URL
	},

	database: {
		username: process.env.DB_USERNAME,
		password: process.env.DB_PASSWORD,
		name: process.env.DB_DATABASE,
		host: process.env.DB_HOST,
		port: process.env.DB_PORT,
		dialect: process.env.DB_DIALECT,
		native: process.env.DB_NATIVE
	}

};
