import _ from 'lodash';

if( _.isNil( process.env.NO_ENVFILE ) ) {
	let dotenv = require( 'dotenv' );
	dotenv.load( { path: ".env" } );
}

module.exports = {

	app: {
		log_level: process.env.LOG_LEVEL || 'ERROR',
		port: 8002,
		name: 'demo-tasks-timer-prod'
	}

};
