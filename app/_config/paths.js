import path from 'path';

export let buildPath = path.resolve('_build');
export let frontendPath = path.resolve('frontend');
export let entryPath = path.resolve('frontend/index');
export let htmlPath = path.resolve('frontend/index.html');
export let faviconPath = path.resolve('frontend/favicon.ico');
export let nodeModulesPath = path.resolve('node_modules');
export let pubicPath = '/';