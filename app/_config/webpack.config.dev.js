import config from 'config';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { entryPath, buildPath, frontendPath, pubicPath, nodeModulesPath, htmlPath, faviconPath } from './paths';

require( "bundle-loader" );

let webConfig = {
	devtool: 'eval-source-map',
	entry: [
		'webpack-dev-server/client?http://localhost:' + config.app.port,
		'webpack/hot/only-dev-server',
		entryPath
	],
	output: {
		path: buildPath,
		filename: 'static/js/bundle.js',
		publicPath: pubicPath,
		pathinfo: true
	},
	devServer: {
		contentBase: frontendPath,
		historyApiFallback: true,
		hot: true,
		port: config.app.port,
		publicPath: pubicPath,
		noInfo: false
	},
	node: {
		net: 'empty',
		tls: 'empty',
		dns: 'empty'
	},
	resolve: {
		extensions: [ '.js', '.jsx', '.json' ],
		alias: {
			'react/lib/ReactMount': 'react-dom/lib/ReactMount'
		}
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				loader: 'eslint-loader',
				include: [ frontendPath ],
				exclude: [ nodeModulesPath ],
				enforce: "pre"
			},
			{
				test: /\.(js|jsx)$/,
				loader: 'react-hot-loader',
				include: [ frontendPath ]
			},
			{
				test: /\.(js|jsx)$/,
				include: [ frontendPath ],
				exclude: [ nodeModulesPath ],
				loader: 'babel-loader',
				query: {
					babelrc: false,
					cacheDirectory: true,
					presets: [ 'babel-preset-es2015', 'babel-preset-es2016', 'babel-preset-react' ],
					plugins: [
						"babel-plugin-transform-decorators-legacy",
						'babel-plugin-syntax-trailing-function-commas',
						'babel-plugin-transform-class-properties',
						'babel-plugin-transform-object-rest-spread',
						[ 'babel-plugin-transform-runtime', { helpers: false, polyfill: false, regenerator: true } ]
					]
				}
			},
			{
				test: /\.scss$/,
				loader: 'style-loader!css-loader!sass-loader',
				include: [ frontendPath ],
			},
			{
				test: /\.css$/,
				include: [ frontendPath ],
				loader: 'style-loader!css-loader'
			},
			{
				test: /\.json$/,
				include: [ frontendPath ],
				loader: 'json-loader',
				options: {
					name: 'static/json/[name].[ext]'
				}
			},
			{
				test: /\.(png|jpg|gif|woff|woff2|mp4|ogg|svg)$/,
				include: [ frontendPath ],
				loader: 'file-loader',
				options: {
					name: 'static/media/[name].[ext]'
				}
			}
		]
	},
	plugins: [
		new webpack.EnvironmentPlugin( [ 'NODE_ENV', 'BASE_URL', 'BASE_API_URL' ] ),
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NamedModulesPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new HtmlWebpackPlugin( {
			inject: true,
			template: htmlPath,
			favicon: faviconPath
		} )
	]
};

export default webConfig;