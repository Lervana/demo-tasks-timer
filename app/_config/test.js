module.exports = {
	allow_requests_retry: process.env.ALLOW_REQUESTS_RETRY,

	app: {
		log_level: process.env.LOG_LEVEL || 'TRACE',
		port: process.env.SERVER_PORT,
		name: 'demo-tasks-timer-test'
	},

	database: {
		name: 'test' + process.env.DB_DATABASE
	}
};
